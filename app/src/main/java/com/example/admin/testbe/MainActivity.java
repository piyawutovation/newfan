package com.example.admin.testbe;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothHeadset;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.ParcelUuid;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static android.bluetooth.BluetoothDevice.DEVICE_TYPE_CLASSIC;
import static android.bluetooth.BluetoothDevice.DEVICE_TYPE_DUAL;
import static android.bluetooth.BluetoothDevice.DEVICE_TYPE_LE;

public class MainActivity extends AppCompatActivity {
    public BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    ConnectThread connectThread;

    UUID DEFAULT_SPP_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    //                                       00001101-0000-1000-8000-00805f9b34fb

    final int REQUEST_ENABLE_BT = 11;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        if (mBluetoothAdapter == null) {
            // Device does not support Bluetooth
            Log.d("xxx","No BE");
        }
        else {
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
            else {
                Log.d("xxx","Passed all");

                mBluetoothAdapter.getProfileProxy(this,mProfileListener,BluetoothProfile.HEADSET);

                Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

                if (pairedDevices.size() > 0) {
                    // There are paired devices. Get the name and address of each paired device.
                    for (BluetoothDevice device : pairedDevices) {
                        Log.d("xxx","================");
                        String deviceName = device.getName();
                        Log.d("xxx","deviceName " + deviceName);

                        if (device.getUuids() != null) {
                            for (ParcelUuid u : device.getUuids()) {
                                Log.d("xxx", "- UUID : " + u.toString());
                            }
                        }
                        else {
                            Log.d("xxx","- the device has no UUID");
                        }

                        String deviceHardwareAddress = device.getAddress(); // MAC address
                        Log.d("xxx","MAC Address : " + deviceHardwareAddress);

                        BluetoothClass btClass = device.getBluetoothClass();
                        Log.d("xxx","class:" + btClass.toString()
                                + " major:"
                                + Integer.toHexString(btClass.getMajorDeviceClass())
                                + " ["
                                + getBTMajorDeviceClass(btClass.getMajorDeviceClass())
                                + "]"
                                + " class:"
                                + Integer.toHexString(btClass.getDeviceClass())
                                + "");

                        int btType = device.getType();
                        Log.d("xxx", "BT TYpe : " + btType);
                        String btTypeString = "Unknown";
                        switch (btType) {
                            case DEVICE_TYPE_CLASSIC :
                                btTypeString = "classic";
                                break;
                            case DEVICE_TYPE_DUAL :
                                btTypeString = "both classic & LE";
                                break;
                            case DEVICE_TYPE_LE :
                                btTypeString = "LE";
                                break;
                        }
                        Log.d("xxx", "BT Type : [" + btTypeString + "] (" + btType + ")");

                        if (deviceName.equals("OOPA")) {
                            Log.d("xxx","WHAT I WANT");
                            connectThread = new ConnectThread(device);
                            connectThread.start();
                        }
                    }
                }
                else {
                    Log.d("xxx","no paired device");
                }

                // Register for broadcasts when a device is discovered.
                IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
                registerReceiver(mReceiver, filter);
            }
        }

    }

    private static String getBTMajorDeviceClass(int major){
        switch(major){
            case BluetoothClass.Device.Major.AUDIO_VIDEO: 	return "AUDIO_VIDEO";
            case BluetoothClass.Device.Major.COMPUTER: 		return "COMPUTER";
            case BluetoothClass.Device.Major.HEALTH:			return "HEALTH";
            case BluetoothClass.Device.Major.IMAGING:			return "IMAGING";
            case BluetoothClass.Device.Major.MISC:			return "MISC";
            case BluetoothClass.Device.Major.NETWORKING:		return "NETWORKING";
            case BluetoothClass.Device.Major.PERIPHERAL:		return "PERIPHERAL";
            case BluetoothClass.Device.Major.PHONE:			return "PHONE";
            case BluetoothClass.Device.Major.TOY:				return "TOY";
            case BluetoothClass.Device.Major.UNCATEGORIZED:	return "UNCATEGORIZED";
            case BluetoothClass.Device.Major.WEARABLE:		return "AUDIO_VIDEO";
            default: 							return "unknown!";
        }
    }

    // Create a BroadcastReceiver for ACTION_FOUND.
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d("xxx","received action : [" + action + "]");
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Discovery has found a device. Get the BluetoothDevice
                // object and its info from the Intent.
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                String deviceName = device.getName();
                String deviceHardwareAddress = device.getAddress(); // MAC address
            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == REQUEST_ENABLE_BT) {
            Log.d("xxx","BT enabled some how?");
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Don't forget to unregister the ACTION_FOUND receiver.
        unregisterReceiver(mReceiver);
    }

    public  void manageMyConnectedSocket (BluetoothSocket mmSocket) {
        Log.d("xxx","say something");
        // Log.d("xxx", "max received packet size " + mmSocket.getMaxTransmitPacketSize());
        try {
            OutputStream outputStream = mmSocket.getOutputStream();
            byte[] b = new byte[] {(byte)'a', (byte)'b', (byte)'c', (byte)'d'};
            b[0] = (byte) 'd';
            outputStream.write(b);
            outputStream.close();
        } catch (IOException e) {
            Log.d("xxx","exception getting output stream");
        }
    }

    private class ConnectThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;

        public ConnectThread(BluetoothDevice device) {
            // Use a temporary object that is later assigned to mmSocket
            // because mmSocket is final.
            BluetoothSocket tmp = null;
            mmDevice = device;

            try {
                // Get a BluetoothSocket to connect with the given BluetoothDevice.
                // MY_UUID is the app's UUID string, also used in the server code.
                tmp = device.createRfcommSocketToServiceRecord(DEFAULT_SPP_UUID);
            } catch (IOException e) {
                Log.e("xxx", "Socket's create() method failed", e);
            }

            Log.d("xxx","initialize thread");

            mmSocket = tmp;
        }

        public void run() {
            // Cancel discovery because it otherwise slows down the connection.
            Log.d("xxx", "in run");
            mBluetoothAdapter.cancelDiscovery();

            try {
                // Connect to the remote device through the socket. This call blocks
                // until it succeeds or throws an exception.

                Log.d("xxx","try to connect");

                mmSocket.connect();
                Log.d("xxx","after connect");
            } catch (IOException connectException) {
                // Unable to connect; close the socket and return.
                Log.e("xxx", "IO Exception" + connectException.toString());
                try {
                    mmSocket.close();
                } catch (IOException closeException) {
                    Log.e("xxx", "Could not close the client socket", closeException);
                }
                return;
            }

            Log.d("xxx","seems to be connected?");


            // The connection attempt succeeded. Perform work associated with
            // the connection in a separate thread.
            manageMyConnectedSocket(mmSocket);
        }

        // Closes the client socket and causes the thread to finish.
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e("xxx", "Could not close the client socket", e);
            }
        }
    }


    private static String bluetoothProfileString(int profile) {
        switch (profile) {
            case BluetoothProfile.HEADSET :
                return "Headset";
            case BluetoothProfile.SAP:
                return "SAP";
            case BluetoothProfile.A2DP:
                return "A2DP";
            case BluetoothProfile.HEALTH:
                return "Health";
            default :
                return "Unknown";
        }
    }

    private BluetoothProfile.ServiceListener mProfileListener = new BluetoothProfile.ServiceListener() {
        public void onServiceConnected(int profile, BluetoothProfile proxy) {
            Log.d("xxx","================");
            Log.d("xxx","service connected | Profile "
                    + bluetoothProfileString(profile)
                    + " ("
                    + profile
                    + ") proxy "
                    + proxy.toString()
            );
            int[] states = {
                    BluetoothProfile.STATE_CONNECTED,
                    BluetoothProfile.STATE_CONNECTING,
                    BluetoothProfile.STATE_DISCONNECTED,
                    BluetoothProfile.STATE_DISCONNECTING
            };

            List<BluetoothDevice> devices = proxy.getDevicesMatchingConnectionStates(states);
            for(BluetoothDevice device : devices) {
                Log.d("xxx","- connected device : " + device.getName());
            }
            if (devices.isEmpty()) {
                Log.d("xxx","- no connected devices");
            }
            if (profile == BluetoothProfile.HEADSET) {
                // mBluetoothHeadset = (BluetoothHeadset) proxy;
            }
        }
        public void onServiceDisconnected(int profile) {
            Log.d("xxx","service disconnected");
            if (profile == BluetoothProfile.HEADSET) {
                // mBluetoothHeadset = null;
            }
        }
    };
}
